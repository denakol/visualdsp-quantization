/* ????????? ?????? */
/***************************************************/
#include "def21060.h"
#define N 65536
#define PaleteNumbers 64
#define width 256
#define height 256
//--------------------------------------------------
// Source array
// 0x30000
.SECTION/DM dm_data;
.VAR input[N];





.SECTION/PM pm_k;
.VAR k[4]=0.4375, 0.1875,  0.3125, 0.0625;


//--------------------------------------------------
//--------------------------------------------------
//--------------------------------------------------
// Result array
// 0x24000
.SECTION/PM pm_data;
.VAR output[2];
//--------------------------------------------------
//--------------------------------------------------
// 0x20004 - 0x20007
.SECTION/PM pm_irq_svc;
nop;
    jump start;
nop;
nop;
//--------------------------------------------------
//--------------------------------------------------
// 0x20100 - 0x20200
.SECTION/PM pm_code;  
start:
  	I0 = input; //?????????? ?????? ??????
	I1 = input+1;
	I2 = input+width-1;
 	I3 = input+width;
	I4 = input+width+1;
	M0=1;
	M1 = 1;
	M2=1;
	M3=1;
	M4=1;
	
	B8 = k;
	L8 = @k;
    M8 = 1;
    BIT Set MODE1 0x00008000;//����� fix �������� � ����
    R7 =  64;
	
    LCNTR = height, DO xxx UNTIL LCE; //�� ������
          r6=CurLCNTR;//������� ������
    LCNTR =width, DO yyy UNTIL LCE; 	//�� ������	    
         	F4 = 0.015625;//��������� ���������� �������
  		   	R15 = DM(I0,0);//�������� ������� ����
			Call get_color_cut_color_v2;//�������� ������� ���������� ����� �������� �� �������
			call demo; //��� ������������� ������� � RGB
			Call set_color;	// �������� ���������� ����� � ������ R0 	
			DM(I0,M0) = R15;//������������� ����
			
			nop;
			//��������� �� ������ ������� � ������
			R15 = CurLCNTR;
		    R0 = width;
		    Comp(R15,R0);
		    if eq jump inc;
		    //��������� �� ��������� ������� � ������		  
		    R0=0;
		    Comp(R15,R0);
		    if eq jump inc;
		    //��������� �� ��������� ��� � ������				
		    Comp(R15,R6);
		    if eq jump inc;
		    
		    Call get_error_v2;//�������� ������
		    
			F4 = PM(I8,M8);//�������� �������� k
			I5=I1;	//������������� ����� ������� �������					;
			Call dithering;			
			
			DM(I1,M1) = R15,F4 = PM(I8,M8);//����������� ������������� ������� � �������� �������� k
			I5=I2;//������������� ����� ������� ������ �������		
			Call dithering;
			DM(I2,M2) = R15,F4 = PM(I8,M8);
			
			I5=I3;//������������� ����� ������� �������		
			Call dithering;
			DM(I3,M3) = R15,F4 = PM(I8,M8);
			
			I5=I4;//������������� ����� ������� ������� �������		
			Call dithering;
			DM(I4,M4) = R15;			
		    jump end;
		
		inc:Modify(I1,M1);
		  	Modify(I2,M2);
		    Modify(I3,M3);
		    Modify(I4,M4);
		end:nop;			
		yyy: nop;
	xxx: nop;

jump exit;		

get_color_cut_color_v2:
            R12 = LShift R15  By -16;//�������� �������� �������� ����� � ���� ���� �� ��������
			R15 = LShift R15 By 16;
			R13 = LShift R15  By -24;
 			R15 = LShift R15 By 8;
			R14 = LShift R15  By -24;
			F0= FLOAT R12;
			F1= FLOAT R13;
			F2= FLOAT R14;
			R15=0;
			F8=  F0 * F4;	       	;//�������� �� ��������� ���������� �������
			F9 =  F1 * F4,R8 =Fix F8 by R15;//����������� 	�������� �� ��������� ���������� ������� � ��������� �������������� � ����� ����������� ��������				
			F10 =  F2 * F4,R9 =Fix F9 by R15;
			R10 =Fix F10;

Rts;

demo:
           R8 = R8 *  R7(UUI);
           R9 = R9 * R7(UUI);
           R10 = R10 * R7(UUI);
Rts;

//�������� �������� �������� �����  (R8,R8,R10) => R15
set_color:            
            R3 = 255;            
            R8 = Min(R8,R3);//����� �������� ����� ���� �� ������ 255
            R15 = R15+ R8;
			R15 = LShift R15 By 8;
			R9 = Min(R9,R3);
			R15 =R15+ R9;
			R15 = LShift R15 By 8;
			R10 = Min(R10,R3);
			R15 = R15+ R10;
Rts;	


get_error_v2:
			R12 = R12-R8;
			F0 = Float r12;
		    R13=R13-R9;
		    F1 = Float R13;
			R14=R14-R10;
			F2 = Float R14;
			
Rts;	
	
//�������� �������� �������� ����� � ���� ���� ��  R15 => (F12,F13,F14) 
get_color_v2:
            R12 = LShift R15  By -16;
			R15 = LShift R15 By 16;
			R13 = LShift R15  By -24;
 			R15 = LShift R15 By 8;
			R14 = LShift R15  By -24;
			F12= FLOAT R12;
			F13= FLOAT R13;
			F14= FLOAT R14;

Rts;

//���������� ����� ������ ��������
dithering:
			R15 = DM(I5,0);
			Call get_color_v2;
			Call update_color_v2; 
			R12 = Trunc F12;
			R13 =Trunc F13;
			R14 =Trunc F14;
			Call set_color_v2;
 			
Rts;
	


update_color_v2:
			F8 = F0* F4; //� ������� ����� �������� ���������� ������*���������
			F9 = F1 * F4,F12 = F8 + F12;			
			F10=  F2 * F4,F13= F9 + F13;
			F14=  F10 +F14;
Rts;


//�������� �������� �������� �����  (R12,R13,R14) => R15
set_color_v2:
            R15 = 0;
            R3 = 255;            
            R12 = Min(R12,R3);
            R15 = R15+ R12;
			R15 = LShift R15 By 8;
			R13 = Min(R13,R3);
			R15 =R15+ R13;
			R15 = LShift R15 By 8;
			R14 = Min(R14,R3);
			R15 = R15+ R14;
Rts;	
			
exit: nop;	