﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           
        }



        public Color GetColor(Color colorError, Double k,Color old)
        {
            var NEWblue = Convert.ToInt32((colorError.B)*k);
            var NEWred = Convert.ToInt32((colorError.R)*k);
            var NEWgree = Convert.ToInt32((colorError.G)*k);
            return Color.FromArgb((old.R + NEWred) < 255 ? old.R + NEWred : 255, (old.G + NEWgree) < 255 ? old.G + NEWgree : 255, (old.B + NEWblue) < 255 ? old.B + NEWblue : 255);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Bitmap bmp;
            using (FileStream fs = new FileStream("dsp 256.png", FileMode.Open, FileAccess.Read, FileShare.Read))
                bmp = new Bitmap(fs);

            int width = bmp.Width,
             height = bmp.Height;
            byte[, ,] res = new byte[3, height, width];
            var NOldColors = 256;
            // количество элементов в палитре
            var NNewColors = 4;
            var num = (NOldColors/NNewColors);
            for (int y = 0; y < height - 1; ++y)
            {
                for (int x = 1; x < width - 1; ++x)
                {
                    var oldColor = bmp.GetPixel(x, y);
                    var NEWblue = (oldColor.B) / num;
                    var NEWred = (oldColor.R) / num;
                    var NEWgree = (oldColor.G) / num;
                    var clor = Color.FromArgb(NEWred, NEWgree, NEWblue);
                    bmp.SetPixel(x, y, clor);
                    var colorErro = Color.FromArgb(oldColor.R - NEWred * num, oldColor.G - NEWgree * num, oldColor.B - NEWblue * num);

                    bmp.SetPixel(x + 1, y, GetColor(colorErro, (Double)(7.0 / 16.0), bmp.GetPixel(x + 1, y)));

                  bmp.SetPixel(x - 1, y + 1, GetColor(colorErro, 3.0 / (Double)16.0, bmp.GetPixel(x - 1, y + 1)));
                   bmp.SetPixel(x, y + 1, GetColor(colorErro, (Double)5.0 / (Double)16.0, bmp.GetPixel(x, y + 1)));
                   bmp.SetPixel(x + 1, y + 1, GetColor(colorErro, (Double)1.0 / (Double)16.0, bmp.GetPixel(x + 1, y + 1)));
                }
            }

            for (int y = 0; y < height - 2; ++y)
            {
                for (int x = 1; x < width - 2; ++x)
                {
                    var oldColor = bmp.GetPixel(x, y);
                    var NEWblue = (oldColor.B) * num;
                    var NEWred = (oldColor.R) * num;
                    var NEWgree = (oldColor.G) * num;
                    var clor = Color.FromArgb(NEWred, NEWgree, NEWblue);
                    bmp.SetPixel(x, y, clor);
                }
            }
            bmp.Save("len4naOut.bmp", ImageFormat.Bmp);
        }
    }
   
  
}
